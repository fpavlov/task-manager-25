package ru.t1.fpavlov.tm.exception.user;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 14.01.2022.
 */
public class UserIdEmptyException extends AbstractUserException {

    @NotNull
    public UserIdEmptyException() {
        super("Error! User id is empty");
    }

}
