package ru.t1.fpavlov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 20.12.2021.
 */
public class UserNotFoundException extends AbstractEntityNotFoundException {

    @NotNull
    public UserNotFoundException() {
        super("Error! The user wasn't found");
    }

}
