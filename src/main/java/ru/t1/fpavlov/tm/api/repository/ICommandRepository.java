package ru.t1.fpavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;

/*
 * Created by fpavlov on 04.10.2021.
 */
public interface ICommandRepository {

    void add(@Nullable final AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable final String commandArgument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String commandName);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
