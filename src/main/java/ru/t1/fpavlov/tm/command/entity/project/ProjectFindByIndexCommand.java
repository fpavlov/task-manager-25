package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectFindByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Find project by index and then display it";

    @Nullable
    public static final String NAME = "project-find-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Project entity = this.findByIndex();
        System.out.println(entity);
    }

}
