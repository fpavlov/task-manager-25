package ru.t1.fpavlov.tm.command.entity.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskListByProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "List task by project id";

    @NotNull
    public static final String NAME = "task-list-by-project-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Sort sort = this.askEntitySort();
        @NotNull final String userId = this.getUserId();
        @NotNull final List<Task> tasks = this.getTaskService().findAllByProjectId(userId, projectId, sort);
        this.renderEntities(tasks, "Tasks:");
    }

}
