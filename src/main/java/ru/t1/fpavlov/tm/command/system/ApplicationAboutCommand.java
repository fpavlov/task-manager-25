package ru.t1.fpavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Display info about author";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println(this.getPropertyService().getAuthorName());
        System.out.println(this.getPropertyService().getAuthorEmail());
    }

}
