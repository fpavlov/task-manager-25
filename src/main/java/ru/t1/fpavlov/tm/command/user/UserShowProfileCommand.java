package ru.t1.fpavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Display user profile info";

    @NotNull
    public static final String NAME = "user-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final User user = this.getAuthService().getUser();
        this.renderUser(user);
    }

}
