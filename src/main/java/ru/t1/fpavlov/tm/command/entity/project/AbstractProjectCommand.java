package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.command.entity.AbstractEntityCommand;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public abstract class AbstractProjectCommand extends AbstractEntityCommand {

    @NotNull
    protected final IProjectService getProjectService() {
        return this.getServiceLocator().getProjectService();
    }

    @Nullable
    protected final Project findById() {
        System.out.println("Enter project id");
        @NotNull final String itemId = TerminalUtil.nextLine();
        @NotNull final String userId = this.getUserId();
        return this.getProjectService().findById(userId, itemId);
    }

    @Nullable
    protected final Project findByIndex() {
        System.out.println("Enter project index");
        @NotNull final Integer itemIndex = TerminalUtil.nextInteger();
        @NotNull final String userId = this.getUserId();
        return this.getProjectService().findByIndex(userId, itemIndex - 1);
    }

    protected final void renderEntities(
            @NotNull final List<Project> entities,
            @Nullable final String listName
    ) {
        if (listName != null) System.out.println(listName);
        for (int i = 0; i < entities.size(); i++) {
            System.out.format("|%2d%s%n", i + 1, entities.get(i));
        }
    }

}
