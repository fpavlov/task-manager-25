package ru.t1.fpavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task item = new Task(name);
        item.setUserId(userId);
        return this.add(item);
    }

    @Nullable
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task item = new Task(name, description);
        item.setUserId(userId);
        return this.add(item);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return this.findAll(userId)
                .stream()
                .filter(m -> projectId.equals(m.getId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final Comparator comparator
    ) {
        final List<Task> result = this.findAllByProjectId(userId, projectId);
        if (comparator != null) result.sort(comparator);
        return result;
    }

}
