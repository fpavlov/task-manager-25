package ru.t1.fpavlov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.model.IHasName;

import java.util.Comparator;

/**
 * Created by fpavlov on 26.11.2021.
 */
public enum NameComparator implements Comparator<IHasName> {

    INSTANCE;

    @Override
    public int compare(
            @Nullable final IHasName item1,
            @Nullable final IHasName item2
    ) {
        if (item1 == null || item2 == null) return 0;
        if (item1.getName() == null || item2.getName() == null) return 0;
        return item1.getName().compareTo(item2.getName());
    }

}
