package ru.t1.fpavlov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.fpavlov.tm.exception.field.*;
import ru.t1.fpavlov.tm.exception.user.UserIdEmptyException;
import ru.t1.fpavlov.tm.model.Project;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return this.repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return this.repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project update(
            @Nullable final String userId,
            @Nullable final Project project,
            @Nullable final String name, final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project item = this.findById(userId, id);
        if (item == null) throw new ProjectNotFoundException();
        item.setName(name);
        if (description == null) {
            item.setDescription("");
        } else {
            item.setDescription(description);
        }
        return item;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project item = this.findByIndex(userId, index);
        if (item == null) throw new ProjectNotFoundException();
        item.setName(name);
        if (description == null) {
            item.setDescription("");
        } else {
            item.setDescription(description);
        }
        return item;
    }

    @NotNull
    @Override
    public Project changeStatus(
            @Nullable final String userId,
            @Nullable final Project project,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        if (status == null) throw new StatusEmptyException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project item = this.findById(userId, id);
        if (item == null) throw new ProjectNotFoundException();
        item.setStatus(status);
        return item;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project item = this.findByIndex(userId, index);
        if (item == null) throw new ProjectNotFoundException();
        item.setStatus(status);
        return item;
    }

}
