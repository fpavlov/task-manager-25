package ru.t1.fpavlov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.api.service.IPropertyService;

import java.util.Properties;

/**
 * Created by fpavlov on 29.01.2022.
 */
public class ProperyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "author.name";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "28432";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "9126098451";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public ProperyService() {
        this.properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return this.getStringValue(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return this.getStringValue(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return this.getStringValue(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return this.getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return this.getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key,
                                  @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) {
            return System.getProperties().getProperty(key);
        }
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(key)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return this.getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaulteValue) {
        return Integer.parseInt(this.getStringValue(key, defaulteValue));
    }

}
